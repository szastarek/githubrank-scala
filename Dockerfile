FROM openjdk:12
ADD target/scala-**/github-rank.jar app.jar
ENV GH_TOKEN=${GH_TOKEN}
ENTRYPOINT ["java","-jar","/app.jar"]
