package services.repository

import javax.inject.{Inject, Singleton}
import model.Repository
import play.api.libs.ws.{WSAuthScheme, WSClient}
import utils.GithubApiUrlFactory

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class RepositoryGithubApiProxy @Inject()(ws: WSClient, githubApiUrlFactory: GithubApiUrlFactory)(implicit exec: ExecutionContext) {

  def getOrganizationRepositories(organization: String): Future[Seq[Repository]] = {
    val request = ws.url(githubApiUrlFactory.getOrganizationRepositoriesURL(organization))
    request
      .withAuth("github-rank", sys.env("GH_TOKEN"), WSAuthScheme.BASIC)
      .withQueryStringParameters()
      .get()
      .map(res => if (res.status.equals(200)) res.json.as[Seq[Repository]] else Seq.empty)
  }

}