package services.contributor

import javax.inject.{Inject, Singleton}
import model.{Contributor, Repository}
import play.api.libs.ws.{WSAuthScheme, WSClient}
import utils.GithubApiUrlFactory

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class ContributorGithubApiProxy @Inject()(ws: WSClient, githubApiUrlFactory: GithubApiUrlFactory)(implicit exec: ExecutionContext) {

  def getContributors(repositories: Seq[Repository]): Future[Seq[Contributor]] = {
    Future.traverse(repositories)(getContributors).map(_.flatten)
  }

  private def getContributors(repository: Repository): Future[Seq[Contributor]] = {
    val request = ws.url(githubApiUrlFactory.getOrganizationContributorsURL(repository))
    request
      .withAuth("github-rank", sys.env("GH_TOKEN"), WSAuthScheme.BASIC)
      .get()
      .map(res => if (res.status.equals(200)) res.json.as[Seq[Contributor]] else Seq.empty)
  }

}
