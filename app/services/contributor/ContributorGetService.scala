package services.contributor

import javax.inject.{Inject, Singleton}
import model.Contributor
import services.repository.RepositoryGithubApiProxy
import utils.page.{Page, PageBuilder}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class ContributorGetService @Inject()(repositoryGithubApiProxy: RepositoryGithubApiProxy,
                                      contributorGithubApiProxy: ContributorGithubApiProxy)(implicit exec: ExecutionContext) {

  def getContributors(organization: String, currentPage: Int, itemsOnPage: Int): Future[Page[Contributor]] = {
    val repositories = repositoryGithubApiProxy.getOrganizationRepositories(organization)
    repositories.flatMap(c => contributorGithubApiProxy.getContributors(c))
      .map(f = c => groupAndPaginate(c, currentPage, itemsOnPage))
  }

  private def groupAndPaginate(contributors: Seq[Contributor], currentPage: Int, itemsOnPage: Int): Page[Contributor] = {
    val result = contributors.groupBy(_.login)
      .map(e => Contributor(e._2.map(_.contributions).sum, e._1))
      .toList
      .sortBy(_.contributions)
      .reverse
    new PageBuilder[Contributor](result, currentPage, itemsOnPage).build()
  }
}
