package utils

import config.GithubLimitsConfig
import javax.inject.Inject
import model.Repository
import org.f100ded.scalaurlbuilder.URLBuilder

class GithubApiUrlFactory @Inject()(config: GithubLimitsConfig) {

  private val ITEMS_ON_PAGE_PARAM = "per_page"
  private val GITHUB_API_BASE_URL = "https://api.github.com/"

  def getOrganizationContributorsURL(repository: Repository): String = {
    val contributorsLimit = config.contributorsLimit
    URLBuilder(GITHUB_API_BASE_URL + "repos")
      .withPathSegments(repository.fullName)
      .withPathSegments("contributors")
      .withQueryParameters(ITEMS_ON_PAGE_PARAM -> contributorsLimit)
      .toString
  }

  def getOrganizationRepositoriesURL(organization: String): String = {
    val repositoriesLimit = config.repositoriesLimit
    URLBuilder(GITHUB_API_BASE_URL + "orgs")
      .withPathSegments(organization)
      .withPathSegments("repos")
      .withQueryParameters(ITEMS_ON_PAGE_PARAM -> repositoriesLimit)
      .toString
  }
}
