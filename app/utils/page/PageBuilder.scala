package utils.page

import java.math.RoundingMode

import com.google.common.math.IntMath

class PageBuilder[A](elements: List[A], currentPage: Int, itemsOnPage: Int) {
  def build(): Page[A] = {
    val startIndex = currentPage * itemsOnPage
    val endIndex = startIndex + itemsOnPage
    val pageElements = elements.slice(startIndex, endIndex)
    val allPages = IntMath.divide(elements.size, itemsOnPage, RoundingMode.CEILING)
    new Page[A](pageElements, pageElements.size, currentPage, itemsOnPage, elements.size, allPages)
  }
}
