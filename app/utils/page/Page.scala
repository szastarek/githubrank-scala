package utils.page

import model.Contributor
import play.api.libs.json.{Json, Reads, Writes}

case class Page[A](elements: List[A], itemsOnCurrentPage: Int, currentPage: Int, itemsOnPage: Int, allItems: Int, allPages: Int)

object Page {
  implicit val pageJsonFormatReads: Reads[Page[Contributor]] = Json.format[Page[Contributor]]
  implicit val pageJsonFormatWrites: Writes[Page[Contributor]] = Json.format[Page[Contributor]]
}