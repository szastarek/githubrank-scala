package config

import com.typesafe.config.{Config, ConfigFactory}

trait AppConfig {
  protected val config: Config = ConfigFactory.load()
}

