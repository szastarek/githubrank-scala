package config

class GithubLimitsConfig extends AppConfig {
  val repositoriesLimit: String = config.getString("github.limit.repositories")
  val contributorsLimit: String = config.getString("github.limit.contributors")
}
