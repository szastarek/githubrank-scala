package model

import play.api.libs.json.{Json, Reads, Writes}

case class Contributor(contributions: Long, login: String)

object Contributor {
  implicit val repositoryJsonFormatReads: Reads[Contributor] = Json.format[Contributor]
  implicit val repositoryJsonFormatWrites: Writes[Contributor] = Json.format[Contributor]
}
