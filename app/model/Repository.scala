package model

import play.api.libs.json.{Json, Reads}

case class Repository(id: Long, name: String, private val full_name: String) {
  def fullName: String = full_name
}

object Repository {
  implicit val repositoryJsonFormat: Reads[Repository] = Json.format[Repository]
}