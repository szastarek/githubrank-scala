package controllers

import javax.inject._
import play.api.libs.json.Json
import play.api.mvc._
import services.contributor.ContributorGetService

import scala.concurrent.ExecutionContext

@Singleton
class ContributorController @Inject()(cc: ControllerComponents, contributorGetService: ContributorGetService)(implicit exec: ExecutionContext) extends AbstractController(cc) {

  def getContributors(organization: String, currentPage: Int, itemsOnPage: Int): Action[AnyContent] = Action.async {
    contributorGetService.getContributors(organization, currentPage, itemsOnPage)
      .map(contributors => Ok(Json.toJson(contributors)))
  }
}
