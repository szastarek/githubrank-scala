package controllers

import model.Contributor
import org.scalatestplus.play.PlaySpec
import play.api.libs.ws.{WSClient, WSResponse}
import play.api.test.WithServer
import utils.page.Page

import scala.concurrent.Await
import scala.concurrent.duration._

class ContributorControllerTest extends PlaySpec {

  "ContributorController#getContributors" must {
    "return list of contributors when organization exist" in new WithServer {
      val wsClient: WSClient = app.injector.instanceOf[WSClient]
      val response: WSResponse = Await.result(wsClient.url(s"http://localhost:$port/org/octokit/contributors?currentPage=0&itemsOnPage=5").get(), 10.second)
      response.status must equal(200)
      response.json.as[Page[Contributor]].elements must not be empty
    }

    "return empty list when organization not exist" in new WithServer {
      val wsClient: WSClient = app.injector.instanceOf[WSClient]
      val response: WSResponse = Await.result(wsClient.url(s"http://localhost:$port/org/notFound/contributors?currentPage=0&itemsOnPage=5").get(), 10.second)
      response.status must equal(200)
      response.json.as[Page[Contributor]].elements mustBe empty
    }
  }
}
