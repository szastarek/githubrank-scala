package services.contributor

import model.{Contributor, Repository}
import org.scalatestplus.play.PlaySpec
import play.api.Application
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.ws.WSClient
import utils.GithubApiUrlFactory

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext}

class ContributorGithubApiProxyTest extends PlaySpec {

  val application: Application = GuiceApplicationBuilder().build()

  "ContributorGithubApiProxy#getContributors" must {
    "return list of contributors when repository exist" in {
      val executionContext = application.injector.instanceOf[ExecutionContext]
      val wsClient = application.injector.instanceOf[WSClient]
      val githubApiUrlFactory = application.injector.instanceOf[GithubApiUrlFactory]
      val repository = Repository(1L, "octokit.rb", "octokit/octokit.rb")
      val repositoryGithubProxy = new ContributorGithubApiProxy(wsClient, githubApiUrlFactory)(executionContext)
      val result = Await.result(repositoryGithubProxy.getContributors(List(repository)), 10.second)
      result must not be empty
      result mustBe a[Seq[Contributor]]
    }

    "return empty list when repository not exist" in {
      val mockExecutionContext = application.injector.instanceOf[ExecutionContext]
      val wsClient = application.injector.instanceOf[WSClient]
      val githubApiUrlFactory = application.injector.instanceOf[GithubApiUrlFactory]
      val repository = Repository(1L, "notFound", "organization/notFound")
      val repositoryGithubProxy = new ContributorGithubApiProxy(wsClient, githubApiUrlFactory)(mockExecutionContext)
      val result = Await.result(repositoryGithubProxy.getContributors(List(repository)), 10.second)
      result mustBe empty
      result mustBe a[Seq[Contributor]]
    }
  }

}
