package services.contributor

import model.{Contributor, Repository}
import org.mockito.Mockito._
import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play.PlaySpec
import play.api.Application
import play.api.inject.guice.GuiceApplicationBuilder
import services.repository.RepositoryGithubApiProxy

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}

class ContributorGetServiceTest extends PlaySpec with MockitoSugar {

  val application: Application = GuiceApplicationBuilder().build()

  "ContributorGetService#getContributors" should {
    "return contributors" in {
      val executionContext = application.injector.instanceOf[ExecutionContext]
      val mockRepositoryGithubApiProxy = mock[RepositoryGithubApiProxy]
      val mockContributorGithubApiProxy = mock[ContributorGithubApiProxy]
      val contributorGetService = new ContributorGetService(mockRepositoryGithubApiProxy, mockContributorGithubApiProxy)(executionContext)
      val organization = "organization"
      val repository = Repository(1L, "name_1", "full_name_1")
      val secondRepository = Repository(2L, "name_2", "full_name_2")
      val contributor = Contributor(6L, "contributor_1")
      val secondContributor = Contributor(2L, "contributor_2")
      val thirdContributor = Contributor(3L, "contributor_3")
      val sumOfFirstContributor = Contributor(12L, "contributor_1")
      when(mockRepositoryGithubApiProxy.getOrganizationRepositories(organization)) thenReturn Future.successful(Seq(repository, secondRepository))
      when(mockContributorGithubApiProxy.getContributors(List(repository, secondRepository))) thenReturn Future.successful(Seq(contributor, secondContributor, thirdContributor, contributor))

      val result = Await.result(contributorGetService.getContributors(organization, 0, 10), 10.second)

      result.elements must have size 3
      result.elements must contain theSameElementsInOrderAs Seq(sumOfFirstContributor, thirdContributor, secondContributor)
      result.elements mustBe a[Seq[Contributor]]
    }

    "return empty contributors list" in {
      val executionContext = application.injector.instanceOf[ExecutionContext]
      val mockRepositoryGithubApiProxy = mock[RepositoryGithubApiProxy]
      val mockContributorGithubApiProxy = mock[ContributorGithubApiProxy]
      val contributorGetService = new ContributorGetService(mockRepositoryGithubApiProxy, mockContributorGithubApiProxy)(executionContext)
      val organization = "organization"
      when(mockRepositoryGithubApiProxy.getOrganizationRepositories(organization)) thenReturn Future.successful(Seq.empty)
      when(mockContributorGithubApiProxy.getContributors(List())) thenReturn Future.successful(Seq())

      val result = Await.result(contributorGetService.getContributors(organization, 0, 10), 10.second)

      result.elements mustBe empty
      result.elements mustBe a[Seq[Contributor]]
    }

  }

}
