package services.repository

import model.Repository
import org.scalatestplus.play.PlaySpec
import play.api.Application
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.ws.WSClient
import utils.GithubApiUrlFactory

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext}

class RepositoryGithubApiProxyTest extends PlaySpec {

  val application: Application = GuiceApplicationBuilder().build()

  "RepositoryGithubApiProxy#getOrganizationRepositories" must {
    "return list of repositories when organization exist" in {
      val ExecutionContext = application.injector.instanceOf[ExecutionContext]
      val wsClient = application.injector.instanceOf[WSClient]
      val githubApiUrlFactory = application.injector.instanceOf[GithubApiUrlFactory]
      val repositoryGithubProxy = new RepositoryGithubApiProxy(wsClient, githubApiUrlFactory)(ExecutionContext)
      val result = Await.result(repositoryGithubProxy.getOrganizationRepositories("octokit"), 10.second)
      result must not be empty
      result mustBe a[Seq[Repository]]
    }

    "return empty list when organization not exist" in {
      val mockExecutionContext = application.injector.instanceOf[ExecutionContext]
      val wsClient = application.injector.instanceOf[WSClient]
      val githubApiUrlFactory = application.injector.instanceOf[GithubApiUrlFactory]
      val repositoryGithubProxy = new RepositoryGithubApiProxy(wsClient, githubApiUrlFactory)(mockExecutionContext)
      val result = Await.result(repositoryGithubProxy.getOrganizationRepositories("notFound"), 10.second)
      result mustBe empty
      result mustBe a[Seq[Repository]]
    }
  }

}
